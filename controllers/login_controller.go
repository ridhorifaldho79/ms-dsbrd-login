package controllers

import (
	"encoding/json"
	"fmt"
	"ms-briapi-dashboard-login/auth"
	"ms-briapi-dashboard-login/middlewares"
	"ms-briapi-dashboard-login/models"
	"ms-briapi-dashboard-login/services"
	"ms-briapi-dashboard-login/utils"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

type LoginDrupalController struct {
	loginDrupalService services.LoginServiceInterface
}

func CreateLoginController(router *gin.Engine, loginDrupalService services.LoginServiceInterface) {
	inDB := LoginDrupalController{loginDrupalService}

	//router.Use(middlewares.CORSMiddleware())
	router.POST("/login", inDB.Login)
	router.POST("/logout", middlewares.TokenAuthMiddleware(), inDB.Logout)
}

func (l *LoginDrupalController) Login(c *gin.Context) {
	key := os.Getenv("KEY_DECRYPT")
	var encrpytData models.Decrypt

	err := c.ShouldBindJSON(&encrpytData)
	if err != nil {
		utils.ErrorMessage(c, http.StatusBadRequest, "Ops, Error when bind json from body")
		fmt.Printf("[login Controller] error when encode data enkripsi : %v\n", err)
		return
	}

	decrypt, err := utils.KeyDecrypt(key, encrpytData.Encrypt)
	if err != nil {
		utils.ErrorMessage(c, http.StatusBadRequest, "Ops, Something went wrong")
		fmt.Printf("[login Controller] error when decrypt data enkripsi : %v\n", err)
		return
	}

	fmt.Println(decrypt)

	result := models.User{}
	err = json.Unmarshal([]byte(decrypt), &result)
	if err != nil {
		utils.ErrorMessage(c, http.StatusInternalServerError, "Ops, Something went wrong")
		fmt.Printf("[login Controller] error when decrypt data enkripsi to struct : %v\n", err)
		return
	}

	checkEmail := utils.CheckValidMail(result.Username)
	if !checkEmail {
		fmt.Println("Invalid email")
		utils.ErrorMessage(c, http.StatusBadRequest, "Invalid email")
		return
	}

	au, err := l.loginDrupalService.LoginDrupal()
	if err != nil {
		utils.ErrorMessage(c, http.StatusBadRequest, "Ops, error when login service")
		fmt.Printf("[login Controller] error when login service : %v\n", err)
		return
	}

	if au.Username != result.Username {
		utils.ErrorMessage(c, http.StatusBadRequest, "Ops, email not available")
		fmt.Printf("[login Controller] email not available : %v\n", err)
		return
	}

	//since after the usefmt.Println(au.Username)r logged out, we destroyed that record in the database so that same jwt token can't be used twice. We need to create the token again
	authData, err := l.loginDrupalService.CreateAuth(result.Username, au.SessID, au.SessionName, au.Token, au.Data)
	if err != nil {
		utils.ErrorMessage(c, http.StatusInternalServerError, err.Error())
		return
	}

	var authD models.Auth
	authD.AuthUUID = authData.AuthUUID
	authD.Username = authData.Username
	authD.SessID = authData.SessID
	authD.SessionName = authData.SessionName

	token, loginErr := l.loginDrupalService.SignIn(authD)
	if loginErr != nil {
		utils.ErrorMessage(c, http.StatusForbidden, "Please try to login later")
		return
	}

	var JWT models.TokenStruct

	JWT.Token = token

	c.JSON(http.StatusOK, JWT)

}

func (l *LoginDrupalController) Logout(c *gin.Context) {
	au, err := auth.ExtractTokenAuth(c.Request)
	if err != nil {
		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}
	fmt.Println(au.Username)

	


	deleted, errs := l.loginDrupalService.DeleteAuthData(au.AuthUUID)
	if errs != nil || deleted != 0 { //if any goes wrong
		c.JSON(http.StatusUnauthorized, "unauthorized")
		return
	}
	utils.SuccessMessage(c, http.StatusOK, "Successfully logged out")
}
