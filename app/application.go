package app

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/subosito/gotenv"
	"github.com/gin-contrib/cors"
	"ms-briapi-dashboard-login/controllers"
	"ms-briapi-dashboard-login/driver"
	"ms-briapi-dashboard-login/repo"
	"ms-briapi-dashboard-login/services"
	"os"
)

func init() {
	gotenv.Load()
}

func StartApp() {
	port := os.Getenv("PORT_LOGIN")
	router := gin.New()
	db := driver.ConnectDB()
	router.Use(gin.Logger())
	router.Use(gin.Recovery())
	router.Use(cors.Default())
	defer db.Close()

	loginRepo := repo.CreateLoginRepoImpl(db)
	loginService := services.CreateLoginService(loginRepo)

	controllers.CreateLoginController(router, loginService)

	fmt.Println("about to start the application...")

	router.Run(port)
}
