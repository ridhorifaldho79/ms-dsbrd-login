package utils

import (
	"github.com/gin-gonic/gin"
)

// ErrorMessage ...
func ErrorMessage(c *gin.Context, status int, msg string) *gin.Context {
	c.JSON(status, gin.H{
		"Success": "False",
		"Message": msg,
	})
	return c
}

// HandleSuccess ...
func SuccessMessage(c *gin.Context, status int, msg string) *gin.Context {
	c.JSON(status, gin.H{
		"Success": "True",
		"Message": msg,
	})
	return c
}
