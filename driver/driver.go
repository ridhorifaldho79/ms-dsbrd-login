package driver

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	"log"
	"ms-briapi-dashboard-login/models"
	"os"
)

var db *gorm.DB

func ConnectDB() *gorm.DB {
	conn := os.Getenv("DUMMY_LOGIN")
	db, err := gorm.Open("postgres", conn)

	if err != nil {
		fmt.Println("[CONFIG.ConnectDB] error when connect to database")
		log.Fatal(err)
	}else {
		fmt.Println("SUCCES CONNECT TO DATABASE")
	}

	models.InitTable(db)

	return db
}
