module ms-briapi-dashboard-login

go 1.13

require (
	github.com/badoux/checkmail v0.0.0-20200623144435-f9f80cb795fa
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/gorm v1.9.14
	github.com/subosito/gotenv v1.2.0
	github.com/twinj/uuid v1.0.0
)
